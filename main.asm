%include "dict.inc"
%include "words.inc"
%include "lib.inc"

section .data
    string: times 256 db 0

section .rodata
    ERROR_MSG: db "Incorrect input", 0 
    NO_WORD: db "Key is not in dict", 0

section .text
global _start
_start:
        mov rdi, string
        mov rsi, 255
        call read_string
        test rax, rax
        jle .er_ms

        mov rdi, string
        mov rsi, pointer
        call find_word
        test rax, rax
        je .err_found
        
        add rax, 8
        push rax
        mov rdi, rax
        call string_length
        pop rcx
        add rax, rcx
        lea rdi, [rax + 1]
        call print_string

        xor rdi, rdi
        call exit

        .err_found:
            mov rdi, NO_WORD
            call print_error_string
            mov rdi, 1
            call exit

        .er_ms:
            mov rdi, ERROR_MSG
            call print_error_string
            mov rdi, 1
            call exit
