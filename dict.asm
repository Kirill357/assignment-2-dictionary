%include "lib.inc"

section .text

global find_word
find_word:
    push rbx
    push rdi
    .loop:
        mov rbx, rsi
        add rsi, 8
        call string_equals;
        cmp rax, 1
        je .win
        cmp qword[rbx], 0
        je .err_found
        mov rsi, [rbx]
        jmp .loop

    .err_found:
        xor rax, rax
        jmp .end
    
    .win:
        mov rax, rbx
        jmp .end

    .end:
        pop rbx
        pop rdi
        ret
