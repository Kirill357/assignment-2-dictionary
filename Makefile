ASM=nasm
ASMFLAGS=-f elf64


main.o: main.asm lib.inc dict.inc colon.inc

dict.o: dict.asm lib.inc

%.o: %.asm %.inc
	$(ASM) $(ASMFLAGS) -o $@ $<


build: main.o lib.o dict.o
	ld -o lab2 main.o lib.o dict.o

MESSAGES := "Hello, World!" "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff" "ddd" "first" "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
test:
    @i=1; \
   	 for message in $(MESSAGES); do \
        	result=$$(echo $$message | ./lab2 2>&1); \
            	echo "Test $$i: $$result"; \
        	i=$$((i+1)); \
   	 done

.PHONY: clean test build
clean: 
	rm -f *.o lab2
